# TODO

This site work on HTML and CSS only.

### Header

- Is always in the upper part of the window, even when you scroll the page
- Has equal space on the left and on the right
- On the left side there is a logo
- In the center there is a search box and a voice search icon
  - When you focus on the search box it should get a border on the input
  - If the screen gets smaller, the search box should decrease in width too
- On the right side there is a notifications icon and a profile icon
  - When you hover over the user’s icon, there should appear the dropdown menu
    - When you hover over the menu items, their background should change;

### Side panel

- Has fixed width and takes up all the available height
- If the window width is less than 1200px, the width of the section should decrease. The shrunken version of the section should contain only the avatars
- The section consists of two smaller parts. The one with streamers is scrollable (if you have enough streamers). The one with information has a fixed height
- The section has the same space on the left and on the right as the header section
- The information section has text (centered) and social icons
  - When you select the text, the selection color and the text color should change
  - When you hover over the icons their color should change
- The section with streamers has a collection of cards with the streamers info
  - When hovering over the card its background color should change
  - The card should have an avatar and a name on the left side and an activity indicator with the number of subscribers/viewers on the right side
  - The activity indicator should have pulse animation
  - The avatar should be round
  - Use text-overflow ellipsis for cases when a name is long;

### The main section

- Takes up all the available space, is scrollable, has the same space (margin/padding) as the header
- Consists of 3 parts
  - Section with categories
    - Has the same height as the height of the first card from the side section
    - Is horizontally scrollable when there are many elements
    - Space between categories is equal
    - Categories should have rounded borders
    - The first category has a different color and background (no need for extra styles for this)
    - It’s preferable to use grid for this section
  - Carousel. This section is **OPTIONAL**, meaning that if you don’t add it your mark won’t be lower. You are free to use any CSS-only carousel, so you don’t have to strictly follow the mockup for this section.
    - The carousel should have video preview (the same as you have in the video section) and a description section on the right side
    - In the description section there should be an avatar, a video title, a streamer’s name and the description itself
  - Video section
    - Has blocks with videos
    - Vertical space between the blocks should be equal
    - Horizontal space between the blocks should be equal
    - When the size of the page decreases the **number of columns should also decrease**
    - This section has to be **responsive and adaptive**
    - You are encouraged to use grid for this section
  - Video container
    - Consists of two parts: preview and description
    - The preview has an image
    - On the preview image there is a “live” badge and a number of active viewers or (in case it's a recording) the length of the video
    - When you hover over the preview the image should darken and a “Play” button should appear. Use fadeIn animation for this effect
    - The description section should have a streamer’s avatar, title of the video/stream, streamer’s name, options icon on the right. Use text-overflow: ellipsis in case you have long titles. When you hover over the text, its color should change and the cursor type should change too.

### Footer

- Is a part of the main section
- Is always at the bottom of the page (even if you have no videos)
- The text in the footer should be centered
- Has space on the top (which separates it from the video section);
